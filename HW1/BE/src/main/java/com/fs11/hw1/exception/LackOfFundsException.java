package com.fs11.hw1.exception;

public class LackOfFundsException extends RuntimeException {
    public LackOfFundsException(String message) {
        super(message);
    }
}