package com.fs11.hw1.controller;

import com.fs11.hw1.entity.Account;
import com.fs11.hw1.entity.Customer;
import com.fs11.hw1.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("customer")
public class CustomerController {
    private final CustomerService service;

    public CustomerController(CustomerService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public Customer getCustomer(@PathVariable Long id) {
        return service.getCustomer(id);
    }

    @GetMapping
    public List<Customer> getAll() {
        return service.getAll();
    }

    @PostMapping
    public Customer createCustomer(@RequestBody Customer customer) {
        return service.create(customer);
    }

    @PutMapping
    public Customer saveCustomer(@RequestBody Customer customer) {
        return service.edit(customer);
    }

    @DeleteMapping("/{id}")
    public void deleteCustomer(@PathVariable("id") Long id) {
        service.deleteCustomer(id);
    }

    @PostMapping("/{id}/account")
    public Customer createAccount(@PathVariable("id") Long id, @RequestBody Account account) {
        return service.createAccount(id, account);
    }

    @DeleteMapping("/{id}/account/{accountId}")
    public void deleteAccount(@PathVariable("id") Long id, @PathVariable("accountId") Long accountId) {
        service.deleteAccount(id, accountId);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({NoSuchElementException.class})
    public String exceptionHandler(NoSuchElementException ex) {
        return ex.getMessage();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({RuntimeException.class})
    public String allErrors(RuntimeException ex) {
        return String.format("An error occurred: %s", ex.getMessage());
    }
}
