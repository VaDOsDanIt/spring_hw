package com.fs11.hw1.dao.impl;

import com.fs11.hw1.dao.Dao;
import com.fs11.hw1.entity.Customer;
import com.fs11.hw1.exception.ElementIsExistException;
import org.springframework.stereotype.Repository;

import java.util.*;


@Repository
public class CustomerDaoImpl implements Dao<Customer> {
    private final List<Customer> customers = new ArrayList<>();


    @Override
    public Customer save(Customer obj) {
        if (customers.contains(obj)) {
            throw new ElementIsExistException(String.format("Customer with id: %d, is already exist", obj.getId()));
        }
        customers.add(obj);
        return obj;
    }

    @Override
    public Customer edit(Customer obj) {
        for (int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getId() == obj.getId()) {
                customers.set(i, obj);
                return obj;
            }
        }
        throw new NoSuchElementException(String.format("Customer with id: %d, not found", obj.getId()));
    }


    @Override
    public boolean delete(Customer obj) {
        return customers.remove(obj);
    }

    @Override
    public void deleteAll(List<Customer> entities) {
        customers.removeAll(entities);
    }

    @Override
    public void saveAll(List<Customer> entities) {
        customers.addAll(entities);
    }

    @Override
    public List<Customer> findAll() {
        return Collections.unmodifiableList(customers);
    }

    @Override
    public boolean deleteById(long id) {
        return customers.removeIf(customer -> id == customer.getId());
    }

    @Override
    public Customer getOne(long id) {
        return customers
                .stream()
                .filter(c -> c.getId() == id)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Customer with id: %d not found", id)));
    }
}
