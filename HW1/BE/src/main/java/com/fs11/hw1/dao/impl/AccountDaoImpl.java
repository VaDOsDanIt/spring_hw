package com.fs11.hw1.dao.impl;

import com.fs11.hw1.dao.Dao;
import com.fs11.hw1.entity.Account;
import com.fs11.hw1.exception.ElementIsExistException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class AccountDaoImpl implements Dao<Account> {
    private final List<Account> accounts = new ArrayList<>();

    @Override
    public Account save(Account obj) {
        if (accounts.contains(obj)) {
            throw new ElementIsExistException(String.format("Account with id: %d, is already exist", obj.getId()));
        }
        accounts.add(obj);
        return obj;
    }

    @Override
    public Account edit(Account obj) {
        for (int i = 0; i < accounts.size(); i++) {
            if (accounts.get(i).getId() == obj.getId()) {
                return accounts.set(i, obj);
            }
        }
        throw new NoSuchElementException(String.format("Account with id: %d, not found", obj.getId()));
    }

    @Override
    public boolean delete(Account obj) {
        return accounts.remove(obj);
    }

    @Override
    public void deleteAll(List<Account> entities) {
        accounts.removeAll(entities);
    }

    @Override
    public void saveAll(List<Account> entities) {
        accounts.addAll(entities);
    }

    @Override
    public List<Account> findAll() {
        return Collections.unmodifiableList(accounts);
    }

    @Override
    public boolean deleteById(long id) {
        return accounts.removeIf(customer -> customer.getId().equals(id));
    }

    @Override
    public Account getOne(long id) {
        return accounts
                .stream()
                .filter(c -> c.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Account with id: %d, not found", id)));
    }

    public Account findByNumber(String number) {
        return accounts.stream()
                .filter(a -> a.getNumber().equals(number))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException(String.format("Account with number: %s, not found", number)));
    }
}
