package com.fs11.hw1.service;

import com.fs11.hw1.dao.impl.AccountDaoImpl;
import com.fs11.hw1.entity.Account;
import com.fs11.hw1.exception.LackOfFundsException;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
    private final AccountDaoImpl dao;

    public AccountService(AccountDaoImpl dao) {
        this.dao = dao;
    }

    public Account topUp(String number, Double amount) {
        if (amount <= 0) throw new IllegalArgumentException("Amount must be > 0");
        Account account = dao.findByNumber(number);
        account.setBalance(amount);
        return dao.edit(account);
    }

    public Account withdraw(String number, Double amount) {
        Account account = dao.findByNumber(number);

        if (amount <= 0) throw new IllegalArgumentException("Amount must be > 0");
        if (account.getBalance() < amount) throw new LackOfFundsException("Lack of money on balance");

        account.setBalance(account.getBalance() - amount);
        return dao.edit(account);
    }

    public Account transfer(String number1, String number2, Double amount) {
        Account account1 = dao.findByNumber(number1);
        Account account2 = dao.findByNumber(number2);

        if (amount <= 0) throw new IllegalArgumentException("Amount must be > 0");
        if (account1.getBalance() < amount) throw new LackOfFundsException("Lack of money on balance");

        account1.setBalance(account1.getBalance() - amount);
        account2.setBalance(account2.getBalance() + amount);

        dao.edit(account2);
        return dao.edit(account1);
    }
}
