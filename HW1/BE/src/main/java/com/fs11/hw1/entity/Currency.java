package com.fs11.hw1.entity;

public enum Currency {
    USD,
    EUR,
    UAH,
    CHF,
    GBP;
}
