package com.fs11.hw1.exception;

public class ElementIsExistException extends RuntimeException {
    public ElementIsExistException(String message) {
        super(message);
    }
}
