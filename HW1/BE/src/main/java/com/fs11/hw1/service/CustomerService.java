package com.fs11.hw1.service;

import com.fs11.hw1.dao.impl.AccountDaoImpl;
import com.fs11.hw1.dao.impl.CustomerDaoImpl;
import com.fs11.hw1.entity.Account;
import com.fs11.hw1.entity.Customer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {
    private final CustomerDaoImpl customerDao;
    private final AccountDaoImpl accountDao;

    private long customerId = 0;
    private long accountId = 0;

    public CustomerService(CustomerDaoImpl dao, AccountDaoImpl accountDao) {
        this.customerDao = dao;
        this.accountDao = accountDao;
    }

    public Customer getCustomer(Long id) {
        return customerDao.getOne(id);
    }

    public List<Customer> getAll() {
        return customerDao.findAll();
    }

    public Customer create(Customer customer) {
        customer.setId(customerId++);
        return customerDao.save(customer);
    }

    public Customer edit(Customer customer) {
        return customerDao.edit(customer);
    }

    public Boolean deleteCustomer(Long id) {
        return customerDao.deleteById(id);
    }

    public Customer createAccount(Long customerId, Account account) {
        account.setId(accountId++);
        Customer customer = customerDao.getOne(customerId).addAccount(account);
        accountDao.save(account);
        return customer;
    }

    public Customer deleteAccount(Long customerId, Long accountId) {
        customerDao.deleteById(accountId);
        return customerDao.getOne(customerId).deleteAccount(accountId);
    }
}
