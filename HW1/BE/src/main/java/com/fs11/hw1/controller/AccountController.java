package com.fs11.hw1.controller;

import com.fs11.hw1.entity.Account;
import com.fs11.hw1.service.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@RestController
@CrossOrigin
@RequestMapping("account")
public class AccountController {
    private final AccountService service;

    public AccountController(AccountService service) {
        this.service = service;
    }

    @PatchMapping("up")
    public Account topUp(@RequestParam(value = "number", required = true) String number,
                         @RequestParam(value = "amount", required = true) Double amount) {
        return service.topUp(number, amount);
    }

    @PatchMapping("withdraw")
    public Account withdraw(@RequestParam(value = "number", required = true) String number,
                            @RequestParam(value = "amount", required = true) Double amount) {
        return service.withdraw(number, amount);
    }

    @PostMapping("transfer")
    public Account transfer(@RequestParam(value = "numberFrom", required = true) String number1,
                            @RequestParam(value = "amount", required = true) Double amount,
                            @RequestParam(value = "numberTo", required = true) String number2) {
        return service.transfer(number1, number2, amount);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({NoSuchElementException.class})
    public String noSuchElementExceptionHandler(NoSuchElementException ex) {
        return ex.getMessage();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({RuntimeException.class})
    public String exceptionHandler(RuntimeException ex) {
        return String.format("An error occurred: %s", ex.getMessage());
    }
}