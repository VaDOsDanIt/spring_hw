import {customerConstants} from "../_const";


const initialState = {
    customers: [],
    isLoadingCustomers: false
}

export const customer = (state = initialState, action) => {
    switch (action.type) {
        case customerConstants.GET_CUSTOMERS_REQUEST:
            return {
                ...state,
                isLoadingCustomers: true
            }
        case customerConstants.GET_CUSTOMERS_SUCCESS:
            return {
                ...state,
                customers: action.payload.customers,
                isLoadingCustomers: false
            }
        default:
            return state;
    }
}
