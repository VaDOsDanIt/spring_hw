import {combineReducers} from "redux";
import {customer} from "./customerReducer";

export const rootReducer = combineReducers({customer});
