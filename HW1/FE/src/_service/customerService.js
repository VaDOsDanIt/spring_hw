export const customerService = {
    getAll
}

function getAll() {
    const requestParams = {
        method: "GET",
        // headers: {
        //     "Content-Types": "application/json"
        // }
    }

    return fetch(`http://localhost:9000/customer`, requestParams);
}
