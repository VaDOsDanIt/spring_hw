import React, {Component} from 'react';
import {connect} from "react-redux";
import {customerAction} from "./_action/customerAction";
import CustomerList from "./_component/CustomerList";

class App extends Component {
    componentDidMount() {
        this.props.dispatch(customerAction.getAll());
    }

    render() {

        const {isLoadingCustomers, customers} = this.props.customer;

        const customersList = !isLoadingCustomers ? <CustomerList customers={customers}/> : <p>Loading...</p>;

        return (
            <div>
                {customersList}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const {customer} = state;
    return {
        customer
    }
}

export default connect(mapStateToProps)(App);
