import {customerService} from "../_service/customerService";
import {customerConstants} from "../_const";

export const customerAction = {
    getAll
}

function getAll() {
    return dispatch => {
        dispatch(request());

        customerService.getAll()
            .then(cc => cc.json())
            .then(json => dispatch(success(json)));
    }

    function request() {
        return {type: customerConstants.GET_CUSTOMERS_REQUEST}
    }

    function success(customers) {
        return {type: customerConstants.GET_CUSTOMERS_SUCCESS, payload: {customers}}
    }
}
