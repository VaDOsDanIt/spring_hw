import React, {Component} from 'react';
import Account from "./Account";

class AccountList extends Component {
    render() {
        const {accounts} = this.props;

        return (
            <div>
                <table cellSpacing={1} border={1}>
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Currency</td>
                        <td>Number</td>
                        <td>Balance</td>
                    </tr>
                    </thead>
                    <tbody>
                    {accounts && accounts.map(account => <Account key={account.id} account={account}/>)}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default AccountList;
