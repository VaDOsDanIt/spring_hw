import React, {Component} from 'react';
import AccountList from "./AccountList";

class Customer extends Component {

    render() {
        const {customer} = this.props;

        return (
            <div>
                <ul>
                    <li>ID: {customer.id}</li>
                    <li>NAME: {customer.name}</li>
                    <li>EMAIL: {customer.email}</li>
                    <li>AGE: {customer.age}</li>
                    <li>Accounts: {<AccountList accounts={customer.accounts}/>}</li>
                </ul>
            </div>
        );
    }
}

export default Customer;
