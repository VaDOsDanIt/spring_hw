import React, {Component} from 'react';

class Account extends Component {
    render() {
        const {account} = this.props;

        return (
            <tr>
                <td>{account.id}</td>
                <td>{account.currency}</td>
                <td>{account.number}</td>
                <td>{account.balance}</td>
            </tr>
        );
    }
}

export default Account;
