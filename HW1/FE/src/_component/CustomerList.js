import React, {Component} from 'react';
import Customer from "./Customer";

class CustomerList extends Component {
    render() {
        const {customers} = this.props;

        return (
            <div className="grid-container">
                {customers && customers.map(customer => <Customer key={customer.id} customer={customer}/>)}
            </div>
        );
    }
}

export default CustomerList;
